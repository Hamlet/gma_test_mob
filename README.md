### G.M.A. Test Mobile
![G.M.A. Test Mobile's screenshot](screenshot.png)  
**_An humanoid mob for testing purposes._**

**Version:** 0.1.1  
**Source code's license:** [EUPL v1.2][1] or later.  
**Texture license:** [CC-BY-SA 4.0 International][2] or later.  
**Models license:** [CC0 1.0 Universal][3] or later.  
**Sounds license:** [CC-BY 3.0][4]

**Dependencies:** gma  


### Installation

Unzip the archive, rename the folder to gma_test_mob and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods


[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://creativecommons.org/licenses/by-sa/4.0/
[3]: https://creativecommons.org/publicdomain/zero/1.0/
[4]: https://creativecommons.org/licenses/by/3.0/