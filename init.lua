--[[

	Copyright © 2018-2019 Hamlet <hamlatmesehub@riseup.net>
	Copyright © 2016, 2018 azekillDIABLO, taikedz, Lone_Wolf

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Mobile definition
--

gma.mobile("test_mob", {
	a = true,
	-- visible - boolean - true/false
	-- Default: true

	b = true,
	-- collide with nodes - boolean - true/false
	-- Default: true

	c = true,
	-- collide with objects - boolean - true/false
	-- Default: true

	d = {-0.35, -1.0, -0.35, 0.35, 0.75, 0.35},
	-- collision box - table -
	-- Default: {-0.35, -0.35, -0.35, 0.35, 0.35, 0.35}

	e = "mesh",
	-- visual type - string
	-- "cube" / "sprite" / "upright_sprite" / "mesh" / "wielditem"
	-- Default: "mesh"

	f = {x = 1.0, y = 1.0},
	-- visual size - table -
	-- Default: {x = 1.0, y = 1.0}

	g = "gma_character.b3d",
	-- mesh name - string -
	-- Default: nil

	h = {"gma_character_template_128x64.png"},
	-- textures - table -
	-- Use {"texture.png"} if your model needs only one texture.
	-- Use {"texture1.png", "texture2.png", etc.} if it needs more than one.
	-- Use {{texture1.png}, {texture2.png}, etc.} to choose a random one.
	-- Default: nil

	i = nil,
	-- colors - table - example: {"red", "blue", "green"}
	-- Default: nil

	j = false,
	-- backface culling - boolean - true/false
	-- Default: false

	k = false,
	-- automatic rotate - boolean - true/false
	-- Default: false

	l = -90.0,
	-- automatic face movement dir - degrees - 0.0 = true, false to disable
	-- Mind that is not 0-359, but 0 -> 180 or 0 -> -180
	-- Default: false

	m = -1.0,
	-- automatic face movement max rotation per sec - degrees - < 0 = unlimited
	-- Default: -1.0

	n = 1.01,
	-- stepheight - float -
	-- Default: 1.01

	o = 5,
	-- weight - float - used to make the mobile fall.
	-- Default: 5

	oa = 50,
	-- chance of movement - integer - 0: stand still; 100: never stop
	-- Default: 50

	ob = 15.0,
	-- direction change interval - float - seconds
	-- Default: 15.0

	p = 25,
	-- max hit points - integer - 2hp = 1 player heart
	-- Default: 20

	q = 15,
	-- min hit points - integer - 2hp = 1 player heart
	-- Default: 20

	qa = 0,
	-- Min light level that the mob can stand. - integer -
	-- If set to -1 then darkness damage is enabled.
	-- Default: 0
	-- Unused unless light or darkness damage is enabled. (zc == true)

	qb = 3,
	-- Max light level that the mob can stand. - integer -
	-- If set to -1 then darkness damage is enabled.
	-- Default: 3
	-- Unused unless light or darkness damage is enabled. (zc == true)

	qc = nil,
	-- Number of hit points to subtract if the mobile is being damaged.
	-- Default: nil
	-- "nil" allows to calculate per mobile damage (initial_hp / 10)
	-- Unused unless light or darkness damage is enabled. (zc == true)

	r = "Test Mob",
	-- name tag - string -
	-- Default: "Test Mob"

	ra = {"humanoids"},
	-- friendly group(s) - table -
	-- E.g. {"humans", "cats", "dogs"}
	-- If set, mobiles will not attack members of this/these group(s).
	-- However - if not passive - will fight back when attacked.
	-- Default: nil.

	rb = nil,
	-- hostile group(s) - table -
	-- E.g. {"rats", "wolves"}
	-- If set, non passive mobiles will attack members of this/these
	-- group(s).
	-- Default: nil.

	rc = nil,
	-- feared group(s) - table -
	-- E.g. {"ghosts", "demons"}
	-- If set, mobiles will flee from members of this/these group(s).
	-- Non passive mobiles will not fight back when attacked.
	-- Default: nil.

	rd = 1,
	-- temper - number - 0: peaceful; 1: neutral; 2: hostile; 3: random
	-- peaceful: if hit doesn't hit back
	-- neutral: if hit it will hit back
	-- hostile: it will hit whenever possible
	-- random: it will be chosen a random value between 0 and 2
	-- Default: 1

	re = false,
	-- team fight - boolean - true/false
	-- requires "ra" to be set
	-- true: will group to fight a common enemy
	-- false: will not group to fight
	-- Default: false

	rf = true,
	-- search for enemies - boolean - true/false
	-- requires "rb" to be set
	-- true: will periodically check within its detection range ("xa")
	-- Default: false

	rf1 = 5.0,
	-- enemies' scan interval - seconds - float -
	-- requires "rb" and/or "rc" to be set
	-- Default: 5.0

	s = "white",
	-- name tag color - ColorSpec -
	-- ../minetest/doc/lua_api.txt at line 2131 (v0.4.17.1)
	-- Default: "white"

	t = "Mobile",
	-- infotext - string - shown when pointing at the mobile
	-- Default: "Mobile"

	u = true,
	-- makes footstep sound - boolean - true/false
	-- Default: true

	-- sounds table
	-- Default: nil
	-- ../minetest/doc/lua_api.txt at line 681 and 2890,
	-- if you want to use random sounds for an event,
	-- check lua_api.txt at line 693.
	--[[
	v = {
		standing = { -- the mob stands still
			a = 100,
			-- Play chance - integer - 1...100 - 1: rarely; 100: always
			-- Default: 100

			b = "gma_test_mob_standing",
			-- Ogg Vorbis file's name

			c = 1.0,
			-- Sound gain - float -
			-- Default: 1.0

			d = 20,
			-- Max hear distance - integer - uses an euclidean metric
			-- Default: 20

			e = false
			-- Loop - boolean - true/false
			-- Default: false
		},

		sitting = { -- the mob sits
			a = 100,
			-- Play chance - integer - 1...100 - 1: rarely; 100: always
			-- Default: 100

			b = "gma_test_mob_sitting",
			-- Ogg Vorbis file's name

			c = 1.0,
			-- Sound gain - float -
			-- Default: 1.0

			d = 20,
			-- Max hear distance - integer - uses an euclidean metric
			-- Default: 20

			e = false
			-- Loop - boolean - true/false
			-- Default: false
		},

		laying = { -- the mob lays
			a = 100,
			-- Play chance - integer - 1...100 - 1: rarely; 100: always
			-- Default: 100

			b = "gma_test_mob_laying",
			-- Ogg Vorbis file's name

			c = 1.0,
			-- Sound gain - float -
			-- Default: 1.0

			d = 20,
			-- Max hear distance - integer - uses an euclidean metric
			-- Default: 20

			e = false
			-- Loop - boolean - true/false
			-- Default: false
		},

		moving = { -- the mob walks, swims or flies
			a = 100,
			-- Play chance - integer - 1...100 - 1: rarely; 100: always
			-- Default: 100

			b = "gma_test_mob_moving",
			-- Ogg Vorbis file's name

			c = 1.0,
			-- Sound gain - float -
			-- Default: 1.0

			d = 20,
			-- Max hear distance - integer - uses an euclidean metric
			-- Default: 20

			e = false
			-- Loop - boolean - true/false
			-- Default: false
		},

		moving_fast = { -- the mob runs, swims or flies faster
			a = 100,
			-- Play chance - integer - 1...100 - 1: rarely; 100: always
			-- Default: 100

			b = "gma_test_mob_moving_fast",
			-- Ogg Vorbis file's name

			c = 1.0,
			-- Sound gain - float -
			-- Default: 1.0

			d = 20,
			-- Max hear distance - integer - uses an euclidean metric
			-- Default: 20

			e = false
			-- Loop - boolean - true/false
			-- Default: false
		},

		jumping = { -- the mob jumps
			a = 100,
			-- Play chance - integer - 1...100 - 1: rarely; 100: always
			-- Default: 100

			b = "gma_test_mob_jumping",
			-- Ogg Vorbis file's name

			c = 1.0,
			-- Sound gain - float -
			-- Default: 1.0

			d = 20,
			-- Max hear distance - integer - uses an euclidean metric
			-- Default: 20

			e = false
			-- Loop - boolean - true/false
			-- Default: false
		},

		attacking = { -- the mob bites, punches, etc.
			a = 100,
			-- Play chance - integer - 1...100 - 1: rarely; 100: always
			-- Default: 100

			b = "gma_test_mob_attacking",
			-- Ogg Vorbis file's name

			c = 1.0,
			-- Sound gain - float -
			-- Default: 1.0

			d = 20,
			-- Max hear distance - integer - uses an euclidean metric
			-- Default: 20

			e = false
			-- Loop - boolean - true/false
			-- Default: false
		},

		attacking_moving = { -- the mob bites, punches, etc. while moving
			a = 100,
			-- Play chance - integer - 1...100 - 1: rarely; 100: always
			-- Default: 100

			b = "gma_test_mob_attacking_moving",
			-- Ogg Vorbis file's name

			c = 1.0,
			-- Sound gain - float -
			-- Default: 1.0

			d = 20,
			-- Max hear distance - integer - uses an euclidean metric
			-- Default: 20

			e = false
			-- Loop - boolean - true/false
			-- Default: false
		}
	},
	--]]

	-- animations table
	w = { -- ../minetest/doc/lua_api.txt at line 3278
		standing = { -- the mob stands still
			{x = 0, y = 79}, -- x: animation start, y: animation end
			frame_speed = 30, -- animation's speed
			frame_blend = 0, -- should make the animation smoother
			frame_loop = true -- if false then  the animation runs just once
		},

		sitting = { -- the mob sits
			{x = 81, y = 161}, -- x: animation start, y: animation end
			frame_speed = 30, -- animation's speed
			frame_blend = 0, -- should make the animation smoother
			frame_loop = true -- if false then  the animation runs just once
		},

		laying = { -- the mob lays
			{x = 162, y = 167}, -- x: animation start, y: animation end
			frame_speed = 30, -- animation's speed
			frame_blend = 0, -- should make the animation smoother
			frame_loop = true -- if false then  the animation runs just once
		},

		moving = { -- the mob walks, swims or flies
			{x = 168, y = 187}, -- x: animation start, y: animation end
			frame_speed = 16.0, -- animation's speed
			frame_blend = 0, -- should make the animation smoother
			frame_loop = true -- if false then  the animation runs just once
		},

		moving_fast = { -- the mob runs, swims or flies faster
			{x = 168, y = 187}, -- x: animation start, y: animation end
			frame_speed = 30, -- animation's speed
			frame_blend = 0, -- should make the animation smoother
			frame_loop = true -- if false then  the animation runs just once
		},

		jumping = { -- the mob jumps
			{x = 173, y = 173}, -- x: animation start, y: animation end
			frame_speed = 0, -- animation's speed
			frame_blend = 0, -- should make the animation smoother
			frame_loop = true -- if false then  the animation runs just once
		},

		attacking = { -- the mob bites, punches, etc.
			{x = 189, y = 198}, -- x: animation start, y: animation end
			frame_speed = 30, -- animation's speed
			frame_blend = 0, -- should make the animation smoother
			frame_loop = true -- if false then  the animation runs just once
		},

		attacking_moving = { -- the mob bites, punches, etc. while moving
			{x = 200, y = 220}, -- x: animation start, y: animation end
			frame_speed = 30, -- animation's speed
			frame_blend = 0, -- should make the animation smoother
			frame_loop = true -- if false then  the animation runs just once
		}
	},

	x = 1200,
	-- life time - seconds -
	-- Default: 1200 (20mins), one standard Minetest Game's day/night cycle.

	xa = 14,
	-- targets' detection range - nodes -
	-- Default: 14

	xb = 4,
	-- melee range - nodes -
	-- Default: 4, standard Minetest Game's weapons range.

	xc = 0.5,
	-- line of sight offset - nodes -
	-- Default: nil
	-- Used for mobiles taller than 1 node.
	-- 0.5 is ok for mobiles tall as the character.

	xd = 1,
	-- moving speed - meters/second
	-- Used when the mob walks around.
	-- Default: 1

	xe = 4,
	-- fast moving speed - meters/second
	-- Used when the mob attacks or flees.
	-- Default: 4

	xf = 100,
	-- min armor level
	-- 0: no damage taken; 100: normal damage taken; 200: double damage taken
	-- Used to choose a random armor level.
	-- Default: 100

	xg = 100,
	-- max armor level
	-- 0: no damage taken; 100: normal damage taken; 200: double damage taken
	-- Used to choose a random armor level.
	-- Default: 100

	xh = 1,
	-- min damage dealt level - 1: 1HP; 2: 2HP; etc.
	-- Used to choose a random - fixed - damage level.
	-- Default: 1

	xi = 1,
	-- max damage dealt level - 1: 1HP; 2: 2HP; etc.
	-- Used to choose a random - fixed - damage level.
	-- Default: 1

	xj = false,
	-- variable damage - boolean - true/false
	-- Used to allow mobiles to deal a variable damage chosen between
	-- the two previous variables.
	-- Default: false

	ya = "air",
	-- Breathes in - string -
	-- Valid values: "air" or "water"
	-- Default: "air"

	yb = true,
	-- Breathes - boolean - true/false
	-- Default: true
	-- Set to false to disable suffocation.

	za = true,
	-- Suffer fire and lava damage - boolean - true/false
	-- Default: true

	zb = false,
	-- Suffer water damage - boolean - true/false
	-- Default: false

	zc = false,
	-- Suffer light or darkness damage - boolean - true/false
	-- Default: false

	zd = true,
	-- Suffer fall damage - boolean - true/false
	-- Default: true

	ze = 50,
	-- Fall damage modifier - integer -
	-- Used to calculate the fall damage amount,
	-- i.e. ((current HP / modifier) * fall speed)^2
	-- Default: 50

	-- Function "on activate"
	-- Arguments: (self, staticdata, dtime_s)
	-- ../minetest/doc/lua_api.txt at line 3924 (v0.4.17.)
	f_a = nil,

	-- Function "on step"
	-- Arguments: (self, dtime)
	-- ../minetest/doc/lua_api.txt at line 3928 (v0.4.17.)
	f_b = nil,

	-- Function "on punch"
	-- Arguments:
	-- (self, puncher, time_from_last_punch, tool_capabilities, dir)
	-- ../minetest/doc/lua_api.txt at line 3932 (v0.4.17.)
	f_c = nil,

	-- Function "on rightclick"
	-- Arguments: (self, clicker)
	-- ../minetest/doc/lua_api.txt at line 3941 (v0.4.17.)
	f_d = function(self)

		local sound_handle = nil

		if (self.gma_current_animation == nil) then
			self.gma_current_animation = 0
		end

		if (self.gma_current_animation < 7) then
			self.gma_current_animation = (self.gma_current_animation + 1)
		else
			self.gma_current_animation = 0
		end

		if (self.gma_current_animation == 1) then
			gma.ApplyAnimation(self, "standing")

			if (sound_handle ~= nil) then
				minetest.sound_stop(sound_handle)
			end

			sound_handle = gma.PlaySound(self, "standing")

		elseif (self.gma_current_animation == 2) then
			gma.ApplyAnimation(self, "sitting")

			if (sound_handle ~= nil) then
				minetest.sound_stop(sound_handle)
			end

			sound_handle = gma.PlaySound(self, "sitting")

		elseif (self.gma_current_animation == 3) then
			gma.ApplyAnimation(self, "laying")

			if (sound_handle ~= nil) then
				minetest.sound_stop(sound_handle)
			end

			gma.PlaySound(self, "laying")

		elseif (self.gma_current_animation == 4) then
			gma.ApplyAnimation(self, "moving")

			if (sound_handle ~= nil) then
				minetest.sound_stop(sound_handle)
			end

			sound_handle = gma.PlaySound(self, "moving")

		elseif (self.gma_current_animation == 5) then
			gma.ApplyAnimation(self, "moving_fast")

			if (sound_handle ~= nil) then
				minetest.sound_stop(sound_handle)
			end

			sound_handle = gma.PlaySound(self, "moving_fast")

		elseif (self.gma_current_animation == 6) then
			gma.ApplyAnimation(self, "jumping")

			if (sound_handle ~= nil) then
				minetest.sound_stop(sound_handle)
			end

			gma.PlaySound(self, "jumping")

		elseif (self.gma_current_animation == 7) then
			gma.ApplyAnimation(self, "attacking")

			if (sound_handle ~= nil) then
				minetest.sound_stop(sound_handle)
			end

			sound_handle = gma.PlaySound(self, "attacking")

		elseif (self.gma_current_animation == 8) then
			gma.ApplyAnimation(self, "attacking_moving")

			if (sound_handle ~= nil) then
				minetest.sound_stop(sound_handle)
			end

			sound_handle = gma.PlaySound(self, "attacking_moving")

		end
	end,

	-- Function "get staticdata"
	-- Arguments: (self)
	-- ../minetest/doc/lua_api.txt at line 3942 (v0.4.17.1)
	f_e = nil
})


--
-- Mobile ABM spawner
--

gma.abm("test_mob", {
	a = {"group:crumbly"},
	-- Spawn on - table -
	-- Examples:
	-- {"default:dirt"} or
	-- {"default:dirt", "default:sand", "default:snow"} or
	-- {"group:crumbly"}
	-- Default: {"group:crumbly"}

	b = {"air"},
	-- Spawn near to - table -
	-- Same as above, if unused then "air" is used as default value.

	c = 45,
	-- Spawning interval - seconds -
	-- Default: 45

	d = 7500,
	-- Spawning chance - number -
	-- 1: always, 2: 50%, 4 = 25%, etc.
	-- Default: 7500

	e = false,
	-- Catch up - boolean - true/false
	-- Whether if unloaded/reloaded mobiles should behave as they
	-- have not been unloaded.
	-- Default: false

	f = 1,
	-- Active Objects Count - integer -
	-- Max number of active objects in the chosen spawn node.
	-- Default: 1

	g = 2,
	-- Active Objects Count Wider - integer -
	-- Max number of active objects in the 28 nodes around around
	-- the chosen spawn node.
	-- Default: 2

	h = 4,
	-- Min light level - integer -
	-- Default: 4

	i = 15,
	-- Max light level - integer -
	-- Default: 15

	l = 2,
	-- Min height - float - use negative values for underground spawning.
	-- Default: 2

	m = 240,
	-- Max height - float - use negative values for underground spawning.
	-- Default: 240

	n = 4700.0,
	-- Min time - float -
	-- 0: midnight; 6000: 6am; 12000: midday; 18000: 6pm; 23999: 11:59pm
	-- Default: 4700; Dawn.

	o = 19250.0,
	-- Max time - float -
	-- 0: midnight; 6000: 6am; 12000: midday; 18000: 6pm; 23999: 11:59pm
	-- Default: 19250; Dusk.

	p = 2,
	-- required space - float -
	-- To be used if the mobiles' collisionbox is higher or wider than
	-- one node.
	-- E.g: 2 = mobile is 2 nodes tall; it will be performed a check
	-- to grant that it has enough vertical space to be spawned.
	-- Horizontal space is always equal to vertical space.
	-- Default: nil
})


--
-- Mobile node spawner
--

-- Arguments: mob name, infotext, inventory texture, height offset
gma.spawner("test_mob", "Test Mob Spawner", "default_dirt.png", 1)


--
-- Minetest engine debug logging
--

if (minetest.settings:get("debug_log_level") == nil)
or (minetest.settings:get("debug_log_level") == "action")
or	(minetest.settings:get("debug_log_level") == "info")
or (minetest.settings:get("debug_log_level") == "verbose")
then
	minetest.log("action", "[Mod] G.M.A. Test Mobile [v0.1.1] loaded.")
end
